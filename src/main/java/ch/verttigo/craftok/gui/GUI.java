package ch.verttigo.craftok.gui;

import ch.verttigo.craftok.gui.Commands.Command_listSrv;
import ch.verttigo.craftok.gui.Listeners.InventoryListener;
import org.bukkit.plugin.java.JavaPlugin;

public final class GUI extends JavaPlugin {

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(new InventoryListener(), this);
        this.getCommand("listSrv").setExecutor(new Command_listSrv());
    }
}
