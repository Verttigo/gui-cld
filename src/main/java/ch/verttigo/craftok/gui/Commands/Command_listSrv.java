package ch.verttigo.craftok.gui.Commands;

import ch.verttigo.craftok.gui.Inventory.serverInv;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_listSrv implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Material material = Material.getMaterial(args[1].toUpperCase());
            serverInv.openInventory((Player) sender, args[0], material);
        }
        return false;
    }
}
