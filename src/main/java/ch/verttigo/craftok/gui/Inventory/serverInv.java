package ch.verttigo.craftok.gui.Inventory;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.ext.bridge.BridgeServiceProperty;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class serverInv {

    public static void openInventory(Player player, String serviceName, Material material) {
        if (CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServices(serviceName).isEmpty()) return;
        Inventory inventory = Bukkit.createInventory(null, 6 * 9, serviceName);
        inventory.setItem(4, addItemStack(material, 1, (byte) 0, "§6" + serviceName, ""));

        for (int i = 9; i < 18; i++) inventory.setItem(i, addItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 11, " "));

        int slot = 18;
        int increment = 0;
        for (ServiceInfoSnapshot s : CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServices(serviceName)) {
            int temp = slot + increment;
            if (temp > (inventory.getSize() - 1)) break;
            if (s.getProperty(BridgeServiceProperty.IS_ONLINE).get()) {
                Integer onlineCount = s.getProperty(BridgeServiceProperty.ONLINE_COUNT).orElse(0);
                Integer maxPlayers = s.getProperty(BridgeServiceProperty.MAX_PLAYERS).orElse(0);
                inventory.setItem(temp, addItemStack(Material.STAINED_CLAY, 1, (byte) 13, "§6" + s.getServiceId().getName(), "", onlineCount + "§8/§c" + maxPlayers, ""));
                increment++;
            }
        }
        player.openInventory(inventory);
    }

    public static ItemStack addItemStack(Material material, int count, byte byt, String displayName, String... lores) {
        ItemStack itemStack = new ItemStack(material, count, byt);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(displayName);
        itemMeta.setLore(Arrays.asList(lores));
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

}
