package ch.verttigo.craftok.gui.Listeners;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryListener implements Listener {
    @EventHandler
    public void onPlayerClickInEnventory(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        if (!CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServices(e.getInventory().getName()).isEmpty()) {
            e.setCancelled(true);
            if (e.getCurrentItem().getItemMeta() == null) return;
            if (e.getCurrentItem().getType() != Material.STAINED_CLAY) return;
            try {
                String srvName = e.getCurrentItem().getItemMeta().getDisplayName();
                ICloudPlayer cp = BridgePlayerManager.getInstance().getOnlinePlayer(player.getUniqueId());
                cp.getPlayerExecutor().connect(srvName.replace("§6", ""));
                player.closeInventory();
            } catch (Exception error) {
                player.closeInventory();
                player.sendMessage("Erreur : " + error);
            }
        }
    }
}
